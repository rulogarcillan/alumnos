//
//  ViewController.swift
//  practica2
//
//  Created by cice on 28/5/19.
//  Copyright © 2019 cice. All rights reserved.
//

import UIKit
import CoreData


class ViewController: UIViewController {
    
    
    var students = [StudentsEntity]()
    var studentSelected : StudentsEntity?
   
    @IBOutlet weak var studentTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        students = DataManagerCoreData().loadStudents()
        studentTableView.register(UITableViewCell.self, forCellReuseIdentifier: "studentCellIdentifier")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        students = DataManagerCoreData().loadStudents()
        self.studentTableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "editStudentSegue" {
            
            if let studentViewController = segue.destination as? StudentViewController {
                studentViewController.student = studentSelected
            }            
        }
    }
  
}



extension ViewController : UITableViewDataSource , UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return students.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let student = students[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "studentCellIdentifier", for : indexPath)
        
        cell.textLabel?.text = student.name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        studentSelected = students[indexPath.row]
    }
}
