//
//  Student.swift
//  practica2
//
//  Created by cice on 28/5/19.
//  Copyright © 2019 cice. All rights reserved.
//

import Foundation

struct Student {
    let id : Int16
    let name : String
    let surname : String
    let age: Int16
    let approved: Bool
}
