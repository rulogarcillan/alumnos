//
//  DataManagerCoreData.swift
//  practica2
//
//  Created by cice on 3/6/19.
//  Copyright © 2019 cice. All rights reserved.
//

import UIKit
import CoreData

class DataManagerCoreData : DataManagerProtocol {
    
    
    var managedObjectContext: NSManagedObjectContext? {        
        let appDelegate = (UIApplication.shared.delegate as? AppDelegate)        
        return appDelegate?.persistentContainer.viewContext
    }
    
    func loadStudents() -> [StudentsEntity] {
        
        guard let managedObjectContext = managedObjectContext else { return [StudentsEntity]()  }
        let studentFecthRequest: NSFetchRequest<StudentsEntity> = StudentsEntity.fetchRequest()
        let student = try? managedObjectContext.fetch(studentFecthRequest) as [StudentsEntity]
        return student ??  [StudentsEntity]()
       
    }
    
    func saveStudent() {
        guard let managedObjectContext = managedObjectContext else { return }
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            }catch {
                print("Error al guardar datos \(error.localizedDescription)")
            }
        }
    }
}
