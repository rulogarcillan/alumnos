//
//  StudentViewController.swift
//  practica2
//
//  Created by cice on 28/5/19.
//  Copyright © 2019 cice. All rights reserved.
//

import UIKit

class StudentViewController: UIViewController {

    var student: StudentsEntity?
    
    @IBOutlet weak var idTxt: UITextField!
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var ageTxt: UITextField!
    @IBOutlet weak var surnameTxt: UITextField!
    @IBOutlet weak var approvedSwitch: UISwitch!
    
    private let dataManagerCoreData = DataManagerCoreData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func SaveBtn(_ sender: Any) {
        
        guard let managedObjectContext = dataManagerCoreData.managedObjectContext else { return }
        let student = StudentsEntity(context: managedObjectContext)
        
        student.name = nameTxt.text
        surnameTxt.text = surnameTxt.text
        student.age = Int16(ageTxt.text ?? "0") ?? 0
        student.aproved = approvedSwitch.isOn
        student.id = Int16(idTxt.text ?? "0") ?? 0
        
        DataManagerCoreData().saveStudent()
        
        navigationController?.popViewController(animated: true)        
        dismiss(animated: true, completion: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        
        guard let student = student else {return}
        idTxt.text = String(student.id)
        nameTxt.text = student.name
        surnameTxt.text = student.surname
        ageTxt.text = String(student.age)
        approvedSwitch.isOn =  student.aproved
    }
}
