//
//  DataManagerProtocol.swift
//  practica2
//
//  Created by cice on 3/6/19.
//  Copyright © 2019 cice. All rights reserved.
//

import Foundation


protocol DataManagerProtocol {
    func loadStudents() -> [StudentsEntity]
    func saveStudent()
}
